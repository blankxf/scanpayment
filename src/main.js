// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import iView from 'iview';
import 'iview/dist/styles/iview.css';



Vue.use(iView);

Vue.config.productionTip = false

// Vue.prototype.siteUrl = "https://test.yintu168.com:8866"

Vue.prototype.siteUrl = "https://pda.aixuepai.com.cn:8866"

import axios from 'axios';
Vue.prototype.axios = axios;

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
})