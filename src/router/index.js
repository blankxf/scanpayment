import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/index'
import Payment from '@/components/payment'
import Transfer from '@/components/transfer'
import TransferApp from '@/components/transferApp'
import TransferWx from '@/components/transferWx'

Vue.use(Router)

export default new Router({
    routes: [{

            path: '/',
            name: 'index',
            component: Index
        }, {
            path: '/payment',
            name: 'payment',
            component: Payment
        },
        {
            path: '/transfer',
            name: 'transfer',
            component: Transfer
        },
        {
            path: '/transferapp',
            name: 'transferapp',
            component: TransferApp
        },
        {
            path: '/transferwx',
            name: 'transferwx',
            component: TransferWx
        }
    ]
})